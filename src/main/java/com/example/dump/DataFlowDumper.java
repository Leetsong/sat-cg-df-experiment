package com.example.dump;

import soot.*;
import soot.options.Options;
import soot.toolkits.graph.BriefUnitGraph;
import soot.toolkits.graph.pdg.HashMutablePDG;
import soot.toolkits.graph.pdg.PDGNode;
import soot.toolkits.graph.pdg.ProgramDependenceGraph;

import java.io.*;
import java.util.*;

@SuppressWarnings("ALL")
public class DataFlowDumper {

    private static SortedMap<String, SortedMap<String, Integer>> mMethodNodeMapping = new TreeMap<>();

    private static synchronized int index(String method, String node) {
        if (!mMethodNodeMapping.containsKey(method)) {
            mMethodNodeMapping.put(method, new TreeMap<>());
        }

        SortedMap<String, Integer> nodeMapping = mMethodNodeMapping.get(method);
        if (!nodeMapping.containsKey(node)) {
            nodeMapping.put(node, nodeMapping.size());
        }

        return nodeMapping.get(node);
    }

    private static class DataflowTransformer extends BodyTransformer {

        SortedMap<String, SortedMap<Integer, SortedSet<Integer>>> mMethodGraphMapping = new TreeMap<>();

        public SortedMap<String, SortedMap<Integer, SortedSet<Integer>>> getMethodGraphMapping() {
            return mMethodGraphMapping;
        }

        @Override
        protected void internalTransform(Body body, String s, Map<String, String> map) {
            try {
                String signature = body.getMethod().getSignature();
                ProgramDependenceGraph pdg = new HashMutablePDG(new BriefUnitGraph(body));

                SortedMap<Integer, SortedSet<Integer>> graph = new TreeMap<>();

                for (PDGNode src : pdg) {
                    int srcIdx = index(signature, src.toString());
                    List<PDGNode> dsts = src.getDependents();
                    SortedSet<Integer> dstIdxs = new TreeSet<>();
                    for (PDGNode dst : dsts) {
                        dstIdxs.add(index(signature, dst.toString()));
                    }
                    graph.put(srcIdx, dstIdxs);
                    synchronized (mMethodGraphMapping) {
                        if (!mMethodGraphMapping.containsKey(signature)) {
                            mMethodGraphMapping.put(signature, graph);
                        }
                    }
                }
            } catch (Exception e) {
                /* skip methods that causes exception */
            }
        }
    }

    private static BodyTransformer transformer() {
        return new DataflowTransformer();
    }

    private static SortedMap<String, SortedMap<Integer, SortedSet<Integer>>> dataFlow(String processDir) {
        soot.G.reset();

        Options.v().set_process_dir(Arrays.asList(processDir));
        Options.v().set_src_prec(Options.src_prec_class);
        Options.v().set_whole_program(true);
        Options.v().set_prepend_classpath(true);
        Options.v().set_allow_phantom_refs(true);
        Options.v().setPhaseOption("cg.spark", "on");
        Options.v().setPhaseOption("cg.spark", "vta:true");

        DataflowTransformer transformer = new DataflowTransformer();
        PackManager.v().getPack("jtp").add(
                new Transform("jtp.dfserial", transformer));

        Scene.v().loadNecessaryClasses();
        PackManager.v().runPacks();

        return transformer.getMethodGraphMapping();
    }

    private static String dump2(SortedMap<String, SortedMap<Integer, SortedSet<Integer>>> graphMapping) {
        StringBuilder buffer = new StringBuilder();

        for (String method : graphMapping.keySet()) {
            SortedMap<Integer, SortedSet<Integer>> graph = graphMapping.get(method);
            buffer.append(String.format("%s\n", method));
            for (int srcIdx : graph.keySet()) {
                Set<Integer> dstIdxs = graph.get(srcIdx);
                if (dstIdxs.size() == 0) {
                    continue;
                }
                buffer.append(String.format("%s>", Integer.toHexString(srcIdx)));
                for (int dstIdx : dstIdxs) {
                    buffer.append(String.format("%s;", Integer.toHexString(dstIdx)));
                }
                buffer.append("\n");
            }
        }

        return buffer.toString();
    }

    private static void stringToFile(String str, String path) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(path))) {
            bufferedWriter.write(str, 0, str.length());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void usage() {
        System.out.println("Usage: dump <dir> <dst>");
        System.out.println(" <dir> a directory containing one or more targets");
        System.out.println(" <dst> output directory");
    }

    public static void main(String[] args) {
        if (args.length < 2) {
            usage();
            System.exit(1);
        }

        File dir = new File(args[0]);
        if (!dir.isDirectory()) {
            System.err.println("Error: <dir> is not a directory. Should be a directory containing one or more targets.");
            usage();
            System.exit(1);
        }

        File dst = new File(args[1]);
        if (!dst.isDirectory()) {
            System.err.println("Error: <dst> is not a directory. Should be a directory.");
            usage();
            System.exit(1);
        }

        for (File f : dir.listFiles()) {
            System.out.println(String.format("Dumping %s", f.getName()));
            stringToFile(dump2(dataFlow(f.getAbsolutePath())),
                    dst.getAbsolutePath() + File.separator + f.getName() + "-df.dmp");
        }
    }
}
