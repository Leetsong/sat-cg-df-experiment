package com.example.dump;

import soot.*;
import soot.jimple.toolkits.callgraph.CallGraph;
import soot.jimple.toolkits.callgraph.Edge;
import soot.options.Options;

import java.util.*;
import java.io.*;


public class CallGraphDumper {

    private static Map<String, Integer> mMethodMapping =
            new HashMap<>(256);

    private static int index(String methodName) {
        if (!mMethodMapping.containsKey(methodName)) {
            mMethodMapping.put(methodName, mMethodMapping.size());
        }
        return mMethodMapping.get(methodName);
    }

    private static String stringIndex(String methodName) {
        return Integer.toHexString(index(methodName));
    }

    private static CallGraph callGraph(String processDir) {
        soot.G.reset();

        Options.v().set_process_dir(Arrays.asList(processDir));
        Options.v().set_src_prec(Options.src_prec_class);
        Options.v().set_whole_program(true);
        Options.v().set_prepend_classpath(true);
        Options.v().set_allow_phantom_refs(true);
        Options.v().setPhaseOption("cg.spark", "on");
        Options.v().setPhaseOption("cg.spark", "vta:true");

        Scene.v().loadNecessaryClasses();
        PackManager.v().getPack("cg").apply();

        return Scene.v().getCallGraph();
    }

    private static String dump(CallGraph cg) {
        StringBuilder buffer = new StringBuilder();

        buffer.append(String.format("%d;", cg.size()));
        for (Edge e : cg) {
            MethodOrMethodContext src = e.getSrc();
            MethodOrMethodContext tgt = e.getTgt();

            String srcIndex = stringIndex(src.method().getName());
            String dstIndex = stringIndex(tgt.method().getName());

            buffer.append(String.format(
                    "%s>%s;",
                    srcIndex,
                    dstIndex));
        }

        return buffer.toString();
    }

    private static String dump2(CallGraph cg) {
        SortedMap<Integer, SortedSet<Integer>> graph = new TreeMap<>();
        for (Edge e : cg) {
            MethodOrMethodContext src = e.getSrc();
            MethodOrMethodContext tgt = e.getTgt();

            int srcIndex = index(src.method().getName());
            int dstIndex = index(tgt.method().getName());

            if (!graph.containsKey(srcIndex)) {
                graph.put(srcIndex, new TreeSet<>());
            }
            graph.get(srcIndex).add(dstIndex);
        }

        StringBuilder buffer = new StringBuilder();
        for (int srcIdx : graph.keySet()) {
            buffer.append(String.format("%s>", Integer.toHexString(srcIdx)));
            for (int dstIdx : graph.get(srcIdx)) {
                buffer.append(String.format("%s;", Integer.toHexString(dstIdx)));
            }
            buffer.append("\n");
        }

        return buffer.toString();
    }

    private static void stringToFile(String str, String path) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(path))) {
            bufferedWriter.write(str, 0, str.length());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void usage() {
        System.out.println("Usage: dump <dir> <dst>");
        System.out.println(" <dir> a directory containing one or more targets");
        System.out.println(" <dst> output directory");
    }

    public static void main(String[] args) {
        if (args.length < 2) {
            usage();
            System.exit(1);
        }

        File dir = new File(args[0]);
        if (!dir.isDirectory()) {
            System.err.println("Error: <dir> is not a directory. Should be a directory containing one or more targets.");
            usage();
            System.exit(1);
        }

        File dst = new File(args[1]);
        if (!dst.isDirectory()) {
            System.err.println("Error: <dst> is not a directory. Should be a directory.");
            usage();
            System.exit(1);
        }

        for (File f : dir.listFiles()) {
            System.out.println(String.format("Dumping %s", f.getName()));
            stringToFile(dump2(callGraph(f.getAbsolutePath())),
                    dst.getAbsolutePath() + File.separator + f.getName() + "-cg.dmp");
        }
    }
}
