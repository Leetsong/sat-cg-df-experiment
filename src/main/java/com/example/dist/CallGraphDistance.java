package com.example.dist;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class CallGraphDistance {

    private static int distance(SortedMap<Integer, SortedSet<Integer>> g1,
                                SortedMap<Integer, SortedSet<Integer>> g2) {
        int dist = 0;
        Set<Integer> g1SrcSet = g1.keySet();
        Set<Integer> g2SrcSet = g2.keySet();

        for (int src : g2SrcSet) {
            // nodes both in g1 and g2
            if (g1SrcSet.contains(src)) {
                Set<Integer> g1DstSet = g1.get(src);
                Set<Integer> g2DstSet = g2.get(src);

                // edge in g2 but not in g1
                for (int dst : g2DstSet) {
                    if (!g1DstSet.contains(dst)) {
                        dist += 1;
                    }
                }

                // edge in g1 but not in g2
                for (int dst : g1DstSet) {
                    if (!g2DstSet.contains(dst)) {
                        dist += 1;
                    }
                }
            }
            // nodes in g2 but not in g1
            else {
                // for new one, add its distance
                dist += g2.get(src).size();
            }
        }

        // nodes in g1, but not in g2
        for (int src : g1SrcSet) {
            if (!g2SrcSet.contains(src)) {
                dist += g1.get(src).size();
            }
        }

        return dist;
    }

    private static SortedMap<Integer, SortedSet<Integer>> fileToGraph(String path) {
        SortedMap<Integer, SortedSet<Integer>> graph = new TreeMap<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(path))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] tokens = line.split(">");
                Integer srcIdx = Integer.parseUnsignedInt(tokens[0], 16);
                String[] dsts = tokens[1].split(";");
                SortedSet<Integer> dstIdxs = new TreeSet<>();

                for (String s : dsts) {
                    dstIdxs.add(Integer.parseUnsignedInt(s, 16));
                }

                graph.put(srcIdx, dstIdxs);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return graph;
    }

    public static void main(String[] args) {
        if (args.length < 2) {
            System.err.println("Usage: dist <src> <dst>");
            System.exit(1);
        }

        System.out.println(distance(fileToGraph(args[0]), fileToGraph(args[1])));
    }
}
