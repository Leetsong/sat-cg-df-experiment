# CG-DF-Experiment

> Use [guava](https://github.com/google/guava) as data set

### Prerequisite

1. Java 8 or higher, download it [here](https://www.oracle.com/technetwork/java/javase/downloads/index-jsp-138363.html)
2. Python 3.x, download it [here](https://www.python.org/downloads/)

### Download

``` sh
$ git clone https://Leetsong@bitbucket.org/Leetsong/sat-cg-df-experiment.git
```

### Test and Run

#### Step 1 - Install and Extract Guava as Data Set

In the root folder of this project, type:

``` sh
# clone guava
$ git clone https://github.com/google/guava.git
# extract all commits to dataset
$ ./extract-commit
# extract all formal (exclude 0.x.y, alpha, beta, rc) versions to dataset
$ ./extract-release
```

#### Step 2 - Run and Generate results

In the root folder of this project, type:

``` sh
# for call graph, to generate results of commits
$ ./cg-run-commits
# for call graph, to generate results of releases
$ ./cg-run-releases
# for data flow, to generate results of commits
$ ./df-run-commits
# for data flow, to generate results of releases
$ ./df-run-releases
```

Additonly, to show the git diff between commits/release, type:

```sh
# for commits
$ ./run-diff-commits
# for releases
$ ./run-diff-releases
```

and see your results in workspace/diff-commits, and workspace/releases
