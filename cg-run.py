#! /bin/python

import os
import sys
import subprocess
import shlex
import shutil


def do_sort(type, arr):
    if type == 'commits':
        # each commits is in the form of 'index-hash'
        return sorted(arr, key=lambda x: int(x.split('-')[0]))
    else:
        return sorted(arr, key=lambda x: x)


def run_command(cmd):
    args = shlex.split(cmd)
    process = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return process.communicate()


def usage():
    print("Usage: python cg-run.py <which>")
    print("  <which> commits, releases")


if len(sys.argv) != 2:
    usage()
    exit(1)
elif sys.argv[1] != "commits" and sys.argv[1] != "releases":
    print("<which> should be commits or releases")
    usage()
    exit(1)

TYPE = sys.argv[1]
CUR_DIR = os.path.curdir
REPO_DIR = os.path.join(CUR_DIR, 'dataset', 'guava', 'android', TYPE)
TARGET_DIRS = do_sort(TYPE, [d for d in os.listdir(REPO_DIR)])
CG_DUMP_DIR = os.path.join('workspace', 'cg-%s' % TYPE)

print('Build project')
run_command('mvn clean package')

print('Creating workspace')
if not os.path.isdir('workspace'):
    os.mkdir('workspace')
elif os.path.isdir(CG_DUMP_DIR):
    shutil.rmtree(CG_DUMP_DIR)
os.mkdir(CG_DUMP_DIR)
run_command('cp target/SAT-CG-DF-Experiment-1.0-SNAPSHOT-jar-with-dependencies.jar workspace/SAT.jar')

print('Dumping call graph for %s' % TYPE)
run_command(
    'java -cp workspace/SAT.jar com.example.dump.CallGraphDumper %s %s' % (REPO_DIR, CG_DUMP_DIR))

print('Generating similarities')
cg_dump_files = do_sort(TYPE, [d for d in os.listdir(CG_DUMP_DIR)])
cg_dump_files = [os.path.join(CG_DUMP_DIR, d) for d in cg_dump_files]
prev_idx = -1
curr_idx = 0
result = [[], []]
for i in range(1, len(TARGET_DIRS)):
    prev_idx = curr_idx
    curr_idx = i
    prev_dump = cg_dump_files[prev_idx]
    curr_dump = cg_dump_files[curr_idx]
    print(">> %s:%s" % (TARGET_DIRS[prev_idx], TARGET_DIRS[curr_idx]))
    sim, _ = run_command(
        'java -cp workspace/SAT.jar com.example.dist.CallGraphDistance "%s" "%s"' % (prev_dump, curr_dump))
    print("   " + str(sim))
    result[0].append("%s:%s" % (TARGET_DIRS[prev_idx], TARGET_DIRS[curr_idx]))
    result[1].append(str(sim).strip())

print('Result:')
print(result)

print('Exit')
